use std::fs::File;
use std::io::{BufRead, BufReader};
use thiserror::Error;

enum Variant {
    TwoValues,
    ThreeValues,
}

type Result<T> = std::result::Result<T, TaskError>;

#[derive(Error, Debug)]
pub enum TaskError {
    #[error("IO Error")]
    IOError(#[from] std::io::Error),
    #[error("Parse string to int error")]
    ParseIntError(#[from] std::num::ParseIntError),
    #[error("No more elements in vector")]
    NoMoreElements,
}

fn get_input_from_file(filename: String) -> Result<Vec<u32>> {
    let file = File::open(filename)?;
    let reader = BufReader::new(file);
    let mut result: Vec<u32> = Vec::new();
    for line in reader.lines() {
        result.push(line?.parse()?);
    }
    Ok(result)
}

fn cut_vector_by_boundary(v: &mut Vec<u32>, equal_to: u32, variant: Variant) {
    let boundary;
    match variant {
        Variant::TwoValues => {
            boundary = equal_to - v.last().unwrap();
        }
        Variant::ThreeValues => {
            let len = v.len();
            boundary = equal_to - v.last().unwrap() - v[len - 2];
        }
    }
    v.retain(|&i| i <= boundary);
}

fn multiplication_result_of_two_elements_with_sum_equal_to(
    input: Vec<u32>,
    equal_to: u32,
) -> Result<u32> {
    let mut cinput = input;
    cinput.sort_by_key(|&k| std::cmp::Reverse(k));
    cut_vector_by_boundary(&mut cinput, 2020, Variant::TwoValues);
    loop {
        if let Some(last) = cinput.pop() {
            for i in cinput.iter() {
                if i + last == equal_to {
                    let result = i * last;
                    return Ok(result);
                }
            }
        } else {
            return Err(TaskError::NoMoreElements);
        }
    }
}

fn multiplication_result_of_three_elements_with_sum_equal_to(
    input: Vec<u32>,
    equal_to: u32,
) -> Result<u32> {
    let mut cinput = input;
    cinput.sort_by_key(|&k| std::cmp::Reverse(k));
    cut_vector_by_boundary(&mut cinput, 2020, Variant::ThreeValues);
    loop {
        if let Some(last) = cinput.pop() {
            for j in cinput.iter().rev() {
                for i in cinput.iter() {
                    if j + i + last == equal_to && j != i {
                        let result = j * i * last;
                        return Ok(result);
                    }
                }
            }
        } else {
            return Err(TaskError::NoMoreElements);
        }
    }
}

fn main() -> Result<()> {
    let input = get_input_from_file("input".to_owned())?;
    println!(
        "Two elements multiplication: {:}\nThree elements multiplication: {:}",
        multiplication_result_of_two_elements_with_sum_equal_to(input.clone(), 2020)?,
        multiplication_result_of_three_elements_with_sum_equal_to(input, 2020)?,
    );
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_boundary_for_two_elements() {
        let mut v = vec![50, 45, 40, 35, 30, 25, 15, 10];
        cut_vector_by_boundary(&mut v, 55, Variant::TwoValues);
        assert_eq!(v, vec![45, 40, 35, 30, 25, 15, 10]);
    }

    #[test]
    fn test_boundary_for_three_elements() {
        let mut v = vec![50, 45, 40, 35, 30, 25, 15, 10];
        cut_vector_by_boundary(&mut v, 55, Variant::ThreeValues);
        assert_eq!(v, vec![30, 25, 15, 10]);
    }

    #[test]
    fn test_multiplication_result_of_two_elements_with_sum_equal_to() {
        let v = vec![50, 45, 40, 35, 30, 25, 15, 10];
        assert_eq!(
            45 * 10,
            multiplication_result_of_two_elements_with_sum_equal_to(v, 55).unwrap()
        );
    }

    #[test]
    fn test_multiplication_result_of_three_elements_with_sum_equal_to() {
        let v = vec![50, 45, 40, 35, 30, 25, 15, 10];
        assert_eq!(
            30 * 15 * 10,
            multiplication_result_of_three_elements_with_sum_equal_to(v, 55).unwrap()
        );
    }
}

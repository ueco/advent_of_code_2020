use std::fs::File;
use std::io::{BufRead, BufReader};
use thiserror::Error;

#[derive(Debug)]
struct PasswordPolicy {
    lower_bound: usize,
    higher_bound: usize,
    requirement: char,
    password: String,
}

impl PasswordPolicy {
    #[allow(dead_code)]
    fn is_valid(&self) -> bool {
        let occurrence = self.password.matches(self.requirement).count();
        occurrence >= self.lower_bound && occurrence <= self.higher_bound
    }

    fn is_valid_new_policy(&self) -> bool {
        let mut charred_password = self.password.chars();
        let fc = charred_password.nth(self.lower_bound - 1).unwrap();
        let sc = charred_password
            .nth(self.higher_bound - self.lower_bound - 1)
            .unwrap();
        (fc == self.requirement && sc != self.requirement)
            || (fc != self.requirement && sc == self.requirement)
    }
}

impl From<String> for PasswordPolicy {
    fn from(s: String) -> Self {
        let mut splitted = s.split_whitespace();
        let mut boundaries = splitted.next().unwrap().split("-");
        let lower_bound = boundaries.next().unwrap().parse().unwrap();
        let higher_bound = boundaries.next().unwrap().parse().unwrap();

        let requirement = splitted
            .next()
            .unwrap()
            .split(":")
            .next()
            .unwrap()
            .chars()
            .nth(0)
            .unwrap();

        let password = splitted.next().unwrap().to_string();

        Self {
            lower_bound,
            higher_bound,
            requirement,
            password,
        }
    }
}

impl PartialEq for PasswordPolicy {
    fn eq(&self, other: &PasswordPolicy) -> bool {
        self.lower_bound == other.lower_bound
            && self.higher_bound == other.higher_bound
            && self.requirement == other.requirement
            && self.password == other.password
    }
}

type Result<T> = std::result::Result<T, TaskError>;

#[derive(Error, Debug)]
pub enum TaskError {
    #[error("IO Error")]
    IOError(#[from] std::io::Error),
    #[error("No more elements in vector")]
    NoMoreElements,
}

fn get_input_from_file(filename: String) -> Result<Vec<PasswordPolicy>> {
    let file = File::open(filename)?;
    let reader = BufReader::new(file);
    let mut result: Vec<PasswordPolicy> = Vec::new();
    for line in reader.lines() {
        let ppolicy = PasswordPolicy::from(line?.to_string());
        if ppolicy.is_valid_new_policy() {
            result.push(ppolicy);
        }
    }
    Ok(result)
}

fn main() -> Result<()> {
    let input = get_input_from_file("input".to_owned())?;
    println!("input: {:?}\ninput len: {}", input, input.len());
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_password_policy_from_string() {
        let policy = "13-15 x: xwxxxjcxxxxxnxxxxs".to_owned();
        assert_eq!(
            PasswordPolicy {
                lower_bound: 13,
                higher_bound: 15,
                requirement: 'x',
                password: "xwxxxjcxxxxxnxxxxs".to_string()
            },
            PasswordPolicy::from(policy)
        );
    }

    #[test]
    fn test_password_policy_is_valid() {
        let policy = "13-15 x: xwxxxjcxxxxxnxxxxs".to_owned();
        assert!(PasswordPolicy::from(policy).is_valid());
        let policy = "13-15 x: xwxxxjcxxxxxnxxxxxxs".to_owned();
        assert!(PasswordPolicy::from(policy).is_valid());
        let policy = "13-15 x: xwxxxjcxxxxxnxxxxxxxs".to_owned();
        assert!(!PasswordPolicy::from(policy).is_valid());
        let policy = "13-15 x: xwxxjcxxxxxnxxxxs".to_owned();
        assert!(!PasswordPolicy::from(policy).is_valid());
    }

    #[test]
    fn test_password_policy_is_valid_new_policy() {
        let policy = "13-15 x: xwxxxjcxxxxxnxxxxs".to_owned();
        assert!(PasswordPolicy::from(policy).is_valid_new_policy());
        let policy = "13-15 x: xwxxxjcxxxxxxxsxxs".to_owned();
        assert!(PasswordPolicy::from(policy).is_valid_new_policy());
        let policy = "13-15 x: xwxxxjcxxxxxnxsxxs".to_owned();
        assert!(!PasswordPolicy::from(policy).is_valid_new_policy());
        let policy = "13-15 x: xwxxxjcxxxxxxxxxxs".to_owned();
        assert!(!PasswordPolicy::from(policy).is_valid_new_policy());
    }
}
